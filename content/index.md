---
title:          "Willkommen"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
special-offer:  false
---

Sind Sie verspannt? Haben Sie Schmerzen? Möchten Sie sich einfach wieder mal etwas gönnen?

Machen Sie von [unseren Angeboten](angebote "Unser Angebot") Gebrauch und besuchen Sie uns in der Gemeinschaftspraxis im [Zentrum](standort "So finden Sie uns") von Liestal.
Zögern Sie nicht uns zu [kontaktieren](anmeldung "Kontakt und Anmeldung").
