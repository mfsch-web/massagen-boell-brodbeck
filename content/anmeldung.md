---
title:          "Anmeldung"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
image:
  file:         "bild-anmeldung.jpg"
  description:  "Sonnenblume auf farbigen Kissen"
---

Telefonische Anmeldung und Auskunft bei Karin Brodbeck und Blanca Böll.

Wir freuen uns über Ihren Anruf.

**[+41 (0)61 508 04 05](tel:+41615080405)**
