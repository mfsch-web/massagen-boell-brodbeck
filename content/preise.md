---
title:          "Preise"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
image:
  file:         "bild-preise.jpg"
  description:  "Visitenkarte, Gutschein, Abo und Fotos"
---

- 30 Minuten: 55 CHF
- 45 Minuten: 75 CHF
- 60 Minuten: 95 CHF
- 6-er Abos mit 10% Rabatt

Geschenkgutscheine auf Anfrage

10% Rabatt für Leute in Ausbildung
