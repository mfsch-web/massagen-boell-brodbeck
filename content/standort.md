---
title:          "Standort"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
image:
  file:         "bild-standort.jpg"
  description:  "Eingang der Praxis"
---

Sie finden unsere Räumlichkeiten an der Kanonengasse 3 in Liestal neben der Ludothek, mitten in der schönen Altstadt.

[Infoblatt zum neuen Standort an der Kanonengasse 3 (PDF)](Massagen-Standort-200601.pdf)

# Adresse

Blanca Böll &amp; Karin Brodbeck  
Kanonengasse 3  
4410 Liestal
