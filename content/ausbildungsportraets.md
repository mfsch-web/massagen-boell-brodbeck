---
title:          "Ausbildungsporträts"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
image:
  file:         "bild-portraets.jpg"
  description:  "Blanca Böll und Karin Brodbeck"
---

# Blanca Böll

- Diplom in Bewegungspädagogik und Massage (GDS/IfB)
- Zertifikat in Muskel-Shitsu & Mobilisationen
- Mitarbeit in Physiotherapie mit Schwerpunkt in medizinischer Trainingstherapie und in einem Wellnesshotel in den Bergen
- unterrichtet seit Sommer 2010 in einem Teilzeitpensum Rhythmik im Kindergarten

# Karin Brodbeck

- Diplom in Bewegungspädagogik & Massage (GDS/IfB) 2008
- Zertifikat in Muskel-Shitsu & Mobilisationen 2007
- Zertifikat in Schwangerschaftsmassage 2015
- Ausbildungsabschluss Zirkus- und Theaterpädagogin 2006
- leitet seit 1999 Kurse und Workshops in verschiedenen Bewegungsbereichen
- arbeitet seit Sommer 2004 als Bewegungsförderlehrerin an einer Sonderschule
