---
title:          "Angebote"
description:    "Breites Massage-Angebot am Bahnhof Liestal."
image:
  file:         "bild-angebote.jpg"
  description:  "Schultermassage"
---

- Ganzkörper-Massage
- Entspannungs-Massage
- Schwangerschafts-Massage
- Teilkörper-Massagen wie
  - Rückenmassage
  - Schulter- / Nackenmassage
  - Gesichts- / Kopfhautmassage
  - Arm- / Handmassage
  - Bein- / Fussmassage

nach Bedarf kombiniert mit

- Mobilisationstechniken
- Muskel-Shiatsu
- Venentraining

Zur Entspannung & Verwöhnung, sowie bei Nacken- oder Kopfschmerzen, Knie- oder Schulterproblemen, Muskelkater, …
