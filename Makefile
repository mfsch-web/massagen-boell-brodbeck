.DEFAULT_GOAL := site
.PHONY: site clean

SITE = public
TOOLS = tools

content = $(patsubst content/%.md,/%,$(wildcard content/*.md) $(wildcard content/*/*.md))
$(info content: $(content))

site: $(SITE)/css/main.css \
    $(patsubst %,$(SITE)%.html,$(content)) \
    $(SITE)/img/bild-index.jpg \
    $(SITE)/img/bild-angebote.jpg \
    $(SITE)/img/bild-portraets.jpg \
    $(SITE)/img/bild-preise.jpg \
    $(SITE)/img/bild-anmeldung.jpg \
    $(SITE)/img/bild-standort.jpg \
    $(SITE)/img/banner.png \
    $(SITE)/img/bg.png \
    $(SITE)/img/bg-bottom.png \
    $(SITE)/img/bg-top.png \
    $(SITE)/sitemap.xml \
    $(SITE)/favicon.ico \
    $(SITE)/Massagen-Schutzmassnahmen-201028.pdf \
    $(SITE)/Massagen-Standort-200601.pdf

clean:
	rm -rf $(SITE) $(TOOLS)

$(SITE)/%.pdf: %.pdf
	cp $< $@

$(SITE)/img/%: images/%
	mkdir -p $(SITE)/img
	cp $< $@

$(SITE)/favicon.ico: images/favicon.ico
	cp $< $@

$(SITE)/sitemap.xml: sitemap.xml
	cp $< $@

$(SITE) $(TOOLS) $(SITE)/kinesiologie:
	mkdir -p $@

$(TOOLS)/dart-sass/sass: | $(TOOLS)
	wget -O $(TOOLS)/dart-sass.tar.gz https://github.com/sass/dart-sass/releases/download/1.24.2/dart-sass-1.24.2-linux-x64.tar.gz
	tar -xzf $(TOOLS)/dart-sass.tar.gz -C $(TOOLS)

$(SITE)/%.html: content/%.md template.html
	mkdir -p $(dir $@)
	pandoc --template=template.html --from=markdown-auto_identifiers --base-header-level=3 $(subst /, --variable=nav-,/$*) --output=$@ $<

$(SITE)/css/main.css: styles/main.scss styles/*.scss | $(SITE) $(TOOLS)/dart-sass/sass
	mkdir -p $(SITE)/css
	tools/dart-sass/sass $< $@
